ARG KIBANA_VERSION=7.1.1

FROM docker.elastic.co/kibana/kibana:${KIBANA_VERSION}

ARG KIBANA_VERSION
ARG LT_VERSION=0.1.31

COPY plugins/ /tmp/plugins
RUN kibana-plugin install https://github.com/sivasamyk/logtrail/releases/download/v${LT_VERSION}/logtrail-${KIBANA_VERSION}-${LT_VERSION}.zip

EXPOSE 5601
